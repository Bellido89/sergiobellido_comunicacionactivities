package com.example.bellido.comunicacioentreactivities


import android.app.Activity
import android.content.Intent
import android.content.res.Resources
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import kotlinx.android.synthetic.main.verificacion.*


class Verificar : AppCompatActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.verificacion)
        var extras: Bundle = intent.extras
        var res: Resources = resources
        var nombre = extras.getString("usuario")
        var texto = res.getString(R.string.aceptar_condicions)
        textoInfo.setText(textoInfo.text.toString() + " " + nombre + " " + texto)

        var botonAceptar = botonAceptar
        botonAceptar.setOnClickListener(this)
        botonRechazar.setOnClickListener(this)
        textoCondiciones.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        var intent = Intent()
        var res = resources

        if (v == botonAceptar) {
            intent.putExtra("resultado", res.getString(R.string.aceptado))
            setResult(RESULT_OK, intent)
            finish()
        } else if (v == botonRechazar) {
            intent.putExtra("resultado", res.getString(R.string.rechazado))
            setResult(RESULT_CANCELED, intent)
            finish()
        } else if (v == textoCondiciones){
            intent = Intent(this, Condiciones::class.java)
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        var intent = Intent()
        var res = resources
        intent.putExtra("resultado", res.getString(R.string.noContesta) )
        setResult(Activity.RESULT_CANCELED, intent)
        finish()
        super.onBackPressed()
    }
}
