package com.example.bellido.comunicacioentreactivities

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import android.support.v7.app.ActionBar;

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var botonVerificar = botonVerificar
        botonVerificar.setOnClickListener(this)

        var actionBar : ActionBar? = supportActionBar
        actionBar!!.setIcon(R.mipmap.ic_launcher)
        actionBar.setDisplayShowHomeEnabled(true)
    }

    override fun onClick(v: View?) {
        var textoNombre = editText

        if (textoNombre.length() == 0) {
            Toast.makeText(this, getResources()
                    .getString(R.string.debe_introducir_usuario),
                    Toast.LENGTH_SHORT).show()


        } else {

                var intent = Intent(this, Verificar::class.java)
                intent.putExtra("usuario", textoNombre.text.toString())
                startActivityForResult(intent,123)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            123 ->  if (resultCode == Activity.RESULT_OK){
                var resultado = editText.text.toString() + " " + data!!.extras.getString("resultado")
                textoResultado.setText(resultado)

            } else if (resultCode == Activity.RESULT_CANCELED){
                var resultado = editText.text.toString() + " " + data!!.extras.getString("resultado")
                textoResultado.setText(resultado)
            }


        }


    }

    }


