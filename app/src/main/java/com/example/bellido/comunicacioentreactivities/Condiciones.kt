package com.example.bellido.comunicacioentreactivities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.webkit.WebView
import kotlinx.android.synthetic.main.condiciones.*
import kotlinx.android.synthetic.main.verificacion.*

/**
 * Created by Bellido on 24/10/2017.
 */

class Condiciones : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.condiciones)
        var wv : WebView = textoCon
        var texto = getString(R.string.condiciones)
        wv.loadDataWithBaseURL(null, texto, "text/html","UTF-8", null)

    }


}

